DOCKER_COMP ?= docker compose
PHP_BIN ?= php

# Docker containers
PHP_CONT = $(DOCKER_COMP) exec php

# Executables
PHP      = $(PHP_CONT) php
COMPOSER = $(PHP_CONT) composer
SYMFONY  = $(PHP_CONT) bin/console

# Misc
.DEFAULT_GOAL = help
.PHONY        = help build up start down logs sh composer vendor sf cc

## —— 🎵 🐳 The Symfony-docker Makefile 🐳 🎵 ——————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —— Docker 🐳 ————————————————————————————————————————————————————————————————
build: ## Builds the Docker images
	@$(DOCKER_COMP) build --pull --no-cache

up: ## Start the docker hub in detached mode (no logs)
	@$(DOCKER_COMP) up --detach

up-dev:
	@$(DOCKER_COMP) -f docker-compose.yml -f docker-compose.override.yml -f docker-compose.debug.yml up

start: build up ## Build and start the containers

down: ## Stop the docker hub
	@$(DOCKER_COMP) down --remove-orphans

logs: ## Show live logs
	@$(DOCKER_COMP) logs --tail=0 --follow

sh: ## Connect to the PHP FPM container
	@$(PHP_CONT) sh

## —— Composer 🧙 ——————————————————————————————————————————————————————————————
composer: ## Run composer, pass the parameter "c=" to run a given command, example: make composer c='req symfony/orm-pack'
	@$(eval c ?=)
	@$(COMPOSER) $(c)

vendor: ## Install vendors according to the current composer.lock file
vendor: c=install --prefer-dist --no-dev --no-progress --no-scripts --no-interaction
vendor: composer

## —— Symfony 🎵 ———————————————————————————————————————————————————————————————
sf: ## List all Symfony commands or pass the parameter "c=" to run a given command, example: make sf c=about
	@$(eval c ?=)
	@$(SYMFONY) $(c)

cc: c=c:c ## Clear the cache
cc: sf

before-push: cbf cs phpstan unit-test

cs: ## Check code style with PHPCS
	 $(PHP_BIN) vendor/bin/phpcs -s --standard=PSR12 src

cbf: ## Fix code style with PHPCBF
	 $(PHP_BIN) vendor/bin/phpcbf --standard=PSR12 src

phpstan: ## Check code with PHPStan
	 $(PHP_BIN) vendor/bin/phpstan analyse src

unit-test: ## Run unit testing
	 $(PHP_BIN) vendor/bin/phpunit -c phpunit.xml

unit-test-coverage: ## Run unit testing
	 $(PHP_BIN) -dxdebug.mode=coverage vendor/bin/phpunit -c phpunit.xml --coverage-html php-unit-coverage

unit-test-debug: ## Run unit testing with xdebug enabled
	 @PHP_INTERPRETER=$(PHP_BIN) PHP_IDE_CONFIG="serverName=gaad-cli" XDEBUG_CONFIG="idekey=PHPSTORM" $(PHP_BIN) -dxdebug.mode=debug -dxdebug.client_port=9003 -dxdebug.client_host=172.17.0.1 vendor/bin/phpunit -c phpunit.xml --testdox

gen-changelog:
	@if [ ! -d "node_modules" ]; then\
        npm i;\
    fi;\
	npm run changelog;
